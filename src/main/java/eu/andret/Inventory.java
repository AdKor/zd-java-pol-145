package eu.andret;

import eu.andret.item.Item;

import java.util.Arrays;

public class Inventory {
    private final Item[] items = new Item[36];

    public Item getItem(int index) {
        return items[index];
    }

    public void setItem(int index, Item item) {
        items[index] = item;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "items=" + Arrays.toString(items) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Inventory inventory)) {
            return false;
        }
        return Arrays.equals(items, inventory.items);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(items);
    }
}
