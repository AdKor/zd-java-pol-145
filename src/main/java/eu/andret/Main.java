package eu.andret;

import eu.andret.item.Helmet;
import eu.andret.item.Item;
import eu.andret.person.*;

public class Main {
    public static void main(String[] args) {
        final Address address = new Address("Kolorowa", "Kraków", "PL");

        final Person person = new Teacher("Andrzej", "Chmiel", address, Gender.MALE, "Math");
        final Student student = new Student("Test", "Testowy", null, Gender.UNSPECIFIED, 1234);
        System.out.println(student.getLastName());

        final LivingEntity entity = new NPC("");

        if (entity instanceof final Player player) {
            System.out.println(player.getLevel());
        }

        final Item item = new Helmet("Magic sword", "", 100, 20, Material.IRON);

        entity.getInventory().setItem(0, item);

        System.out.println(person.getFirstName());

        Teacher t1 = new Teacher("", "", null, Gender.NON_BINARY, "");
        Teacher t2 = new Teacher("", "", null, Gender.FEMALE, "");
        if (t1.equals(t2)) {
            System.out.println("same");
        } else {
            System.out.println("not same");
        }
        System.out.println(t1);

        Movable[] movables = {
                new NPC("Vendor"),
                new Player()
        };
        for (Movable movable : movables) {
            System.out.println(movable.getLocation());
        }

        Textable textable = new Textable() {
            @Override
            public void write(String text) {
                System.out.println(text + text);
            }
        };
        textable.write("abc");

        Student student1 = new Student("", "", null, Gender.MALE, 1) {
            @Override
            public int getIndexNumber() {
                return 0;
            }
        };
        System.out.println(student1.getIndexNumber());

        Speakable speakable = new Speakable() {
            @Override
            public void speak(String text) {
                System.out.println("I'm anonymous: " + text);
            }
        };
        speakable.speak("Hello!");

        LivingEntity[] entities = {
                new Player(),
                new NPC("Villager"),
                new LivingEntity() {
                    @Override
                    public void speak(String text) {
                        System.out.println("anonymous entity: " + text);
                    }
                }
        };
        for (LivingEntity entity1 : entities) {
            entity1.speak("the loop");
        }

        LivingEntity[] livingEntities = new LivingEntity[10];
        for (int i = 0; i < 10; i++) {
            int count = i;
            livingEntities[i] = new LivingEntity() {
                @Override
                public void speak(String text) {
                    System.out.println(count + ", " + text);
                }
            };
        }

        for (LivingEntity livingEntity : livingEntities) {
            livingEntity.speak("the last but not least");
        }
        System.out.println(Gender.MALE.getPl());
        System.out.println(student.getGender().getPl());

        Box<Teacher> box = new Box<>(t1);
        System.out.println(box.getValue());
        print(entity);
    }

    public static <T extends Entity> void print(T o) {
        System.out.println(o.getName());
    }
}
