package eu.andret.person;

public interface Textable {
    void write(String text);
}
