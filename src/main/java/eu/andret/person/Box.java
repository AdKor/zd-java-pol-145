package eu.andret.person;

import java.util.Objects;

public class Box<T> {
    private T value;

    public Box(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Box{" +
                "value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Box<?> box)) {
            return false;
        }
        return Objects.equals(getValue(), box.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }
}
