package eu.andret.item;


import eu.andret.Material;

import java.util.Objects;

public abstract sealed class ArmorItem extends Item permits Helmet, Chestplate, Leggins, Boots {
    private int armor;
    private Material material;

    protected ArmorItem(String name, String description, int value, int armor, Material material) {
        super(name, description, value);
        this.armor = armor;
        this.material = material;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        if (armor >= 0 && armor <= 100) {
            this.value *= armor / 100.;
            this.armor = armor;
        }
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "ArmorItem{" +
                "armor=" + armor +
                ", material=" + material +
                "} " + super.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ArmorItem armorItem)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        return getArmor() == armorItem.getArmor()
                && getMaterial() == armorItem.getMaterial();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getArmor(), getMaterial());
    }
}
