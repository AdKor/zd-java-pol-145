package eu.andret;

public interface Speakable {
    void speak(String text);
}
